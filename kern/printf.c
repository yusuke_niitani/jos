// Simple implementation of cprintf console output for the kernel,
// based on printfmt() and the kernel console's cputchar().

#include <inc/types.h>
#include <inc/stdio.h>
#include <inc/stdarg.h>


static void

// called for every single character of number
// if a string is of nine numbers (int)123456789, then putch will be called 9 times and cnt=9
putch(int ch, int *cnt)
{
	// cnt ++
	cputchar(ch);
	*cnt++;
}



// when calling vprintfmt, it specifies how to print character
// 
int
vcprintf(const char *fmt, va_list ap)
{
	int cnt = 0;

	vprintfmt((void*)putch, &cnt, fmt, ap);
	return cnt;
}


// what is fmt?: format string
// va_list is variables on back of printf arguments
// printf("sometext %d %d", VAR0, VAR1);  va_list[0] = VAR0
// cnt is a number of character in a code 
int
cprintf(const char *fmt, ...)
{
	va_list ap;
	int cnt;

	va_start(ap, fmt);
	cnt = vcprintf(fmt, ap);
	va_end(ap);

	return cnt;
}

